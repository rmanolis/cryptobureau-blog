#Cryptobureau blog


##Description
Cryptobureau blog is a prototype of a blog that uses only digital signature for every action.
To create the digital signature, the user have to use the Chrome's plugin called Cryptobureau. 
The users can register their public keys and create or delete articles.
However only the admin can delete users , add/delete tags and delete other articles.

The administrator, to run cryptobureau, needs to have Go 1.7, nodejs and mongodb.

##Plugins
https://addons.mozilla.org/en-US/firefox/addon/cryptobureau/?src=cb-dl-created

https://chrome.google.com/webstore/detail/cryptobureau/ojcllechgpjhjoaphbbbjonflchipnfk?hl=en-GB

##Installation guide

cd $GOPATH/src 

git clone https://bitbucket.org/rmanolis/cryptobureau-blog.git

cd cryptobureau-blog

In the config.toml file add your public key for the administration

Install mongodb and make sure it is running

sh requirements.sh

go build

./cryptobureau-blog

(to compile the front end)

cd static/app

npm install

npm run build

After it runs successfully, check through mongo the db "cbblog" for a table "user" with a collumn containing "username:admin"

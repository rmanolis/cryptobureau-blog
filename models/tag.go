package models

import (
	"errors"

	"bitbucket.org/rmanolis/cryptobureau-blog/utils"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const TAG_TABLE = "tag"

type Tag struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Title     string        `bson:"title,omitempty"`
	UserID    bson.ObjectId `bson:"user_id,omitempty"`
	Signature string        `bson:"signature,omitempty"`
}

func (t *Tag) Save(db *mgo.Database) error {
	c := db.C(TAG_TABLE)
	t.ID = bson.NewObjectId()
	return c.Insert(&t)
}

func (t *Tag) Destroy(db *mgo.Database) error {
	c := db.C(TAG_TABLE)
	return c.Remove(t)
}
func (t *Tag) Update(db *mgo.Database) error {
	c := db.C(TAG_TABLE)
	return c.UpdateId(t.ID, t)
}

func GetTagByTitle(db *mgo.Database, name string) (*Tag, error) {
	c := db.C(TAG_TABLE)
	tag := new(Tag)
	err := c.Find(bson.M{"title": bson.M{"$eq": name}}).One(&tag)
	return tag, err
}

func GetTagByID(db *mgo.Database, id string) (*Tag, error) {
	c := db.C(TAG_TABLE)
	tag := new(Tag)
	if !bson.IsObjectIdHex(id) {
		return tag, errors.New("tag id is not correct")
	}
	err := c.FindId(bson.ObjectIdHex(id)).One(&tag)
	return tag, err
}

func JsonTags(db *mgo.Database, opts *utils.OptPage, search bson.M) (*utils.PaginationJson, error) {
	c := db.C(TAG_TABLE)
	q := c.Find(search)
	p, err := utils.Paginate(q, opts)
	if err != nil {
		return nil, err
	}
	var as = []Tag{}
	err = p.Query.Sort("-date").All(&as)
	var ajs = []TagJson{}
	for _, v := range as {
		aj := new(TagJson)
		aj.Set(&v)
		ajs = append(ajs, *aj)
	}
	pj := p.ToJson(ajs)
	return pj, err
}

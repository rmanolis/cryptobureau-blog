package models

import (
	"errors"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserJson struct {
	ID        string `json:"id"`
	PublicKey string `json:"public_key"`
	Username  string `json:"username"`
}

func (uj *UserJson) Set(u *User) {
	uj.ID = u.ID.Hex()
	uj.PublicKey = u.PublicKey
	uj.Username = u.Username
}

type SignatureJson struct {
	Text      string `json:"text"`
	Signature string `json:"signature"`
	Username  string `json:"username"`
}

type ArticleToSignJson struct {
	Text string `json:"text"`
}

func (aj *ArticleToSignJson) Set(a *Article) {
	aj.Text = a.Text
}

type ArticleJson struct {
	ID        string    `json:"id"`
	Title     string    `json:"title"`
	Text      string    `json:"text"`
	Username  string    `json:"username"`
	PublicKey string    `json:"public_key"`
	UserID    string    `json:"user_id"`
	Signature string    `json:"signature"`
	Date      time.Time `json:"date"`
	Tags      []string  `json:"tags"`
}

func (aj *ArticleJson) Set(db *mgo.Database, a *Article) {
	aj.ID = a.ID.Hex()
	aj.Text = a.Text
	aj.Title = a.Title
	aj.UserID = a.UserID.Hex()
	aj.Date = a.Date
	aj.Signature = a.Signature
	user, err := GetUserByID(db, a.UserID.Hex())
	if err == nil {
		aj.Username = user.Username
		aj.PublicKey = user.PublicKey
	}
	aj.Tags = []string{}
	for _, v := range a.Tags {
		tag, err := GetTagByID(db, v.Hex())
		if err == nil {
			aj.Tags = append(aj.Tags, tag.Title)
		}
	}
}

type TagJson struct {
	ID       string `json:"id"`
	Text     string `json:"text"`
	UserID   string `json:"user_id"`
	Username string `json:"username"`
}

func (tj *TagJson) Set(t *Tag) {
	tj.ID = t.ID.Hex()
	tj.Text = t.Title
	tj.UserID = t.UserID.Hex()
}

func (tj *TagJson) ToModel() (*Tag, error) {
	t := new(Tag)
	if !bson.IsObjectIdHex(tj.ID) {
		return nil, errors.New("id is not ObjectID")
	}

	t.ID = bson.ObjectIdHex(tj.ID)
	t.Title = tj.Text
	if !bson.IsObjectIdHex(tj.UserID) {
		return nil, errors.New("user id is not ObjectID")
	}

	t.UserID = bson.ObjectIdHex(tj.UserID)
	return t, nil
}

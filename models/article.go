package models

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/rmanolis/cryptobureau-blog/utils"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const ARTICLE_TABLE = "article"

type Article struct {
	ID        bson.ObjectId   `bson:"_id,omitempty"`
	Title     string          `bson:"title,omitempty"`
	Text      string          `bson:"text,omitempty"`
	Signature string          `bson:"signature,omitempty"`
	Date      time.Time       `bson:"date,omitempty"`
	UserID    bson.ObjectId   `bson:"user_id,omitempty"`
	Tags      []bson.ObjectId `bson:"tags,omitempty"`
}

func (art *Article) Save(db *mgo.Database) error {
	c := db.C(ARTICLE_TABLE)
	art.ID = bson.NewObjectId()
	art.Date = time.Now()
	return c.Insert(&art)
}

func (art *Article) Destroy(db *mgo.Database) error {
	c := db.C(ARTICLE_TABLE)
	return c.Remove(&art)
}

func (a *Article) Update(db *mgo.Database) error {
	c := db.C(ARTICLE_TABLE)
	return c.UpdateId(a.ID, a)
}

func GetArticleByID(db *mgo.Database, id string) (*Article, error) {
	c := db.C(ARTICLE_TABLE)
	art := new(Article)
	if !bson.IsObjectIdHex(id) {
		return art, errors.New("article id is not correct")
	}
	err := c.FindId(bson.ObjectIdHex(id)).One(&art)
	return art, err
}

func DeleteUnsignedArticles(db *mgo.Database) error {
	c := db.C(ARTICLE_TABLE)
	arts := []Article{}

	err := c.Find(bson.M{"signature": nil}).All(&arts)
	for _, v := range arts {
		after := v.Date.Add(time.Minute * 8)
		if !after.After(time.Now()) {
			fmt.Println("Deleting unsigned article ", v)
			v.Destroy(db)
		}
	}
	return err
}

func JsonArticles(db *mgo.Database, opts *utils.OptPage, search bson.M) (*utils.PaginationJson, error) {
	c := db.C(ARTICLE_TABLE)
	q := c.Find(search)
	p, err := utils.Paginate(q, opts)
	if err != nil {
		return nil, err
	}
	var as = []Article{}
	err = p.Query.Sort("-date").All(&as)
	var ajs = []ArticleJson{}
	for _, v := range as {
		aj := new(ArticleJson)
		aj.Set(db, &v)
		ajs = append(ajs, *aj)
	}
	pj := p.ToJson(ajs)
	return pj, err
}

package models

import (
	"errors"
	"time"

	"bitbucket.org/rmanolis/cryptobureau-blog/utils"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const USER_TABLE = "user"

type User struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Username  string        `bson:"username,omitempty"`
	IsAdmin   bool          `bson:"is_admin,omitempty"`
	PublicKey string        `bson:"public_key,omitempty"`
	Signature string        `bson:"signature,omitempty"` //to sign the ID of the user
	Date      time.Time     `bson:"date,omitempty"`
}

func (u *User) Save(db *mgo.Database) error {
	c := db.C(USER_TABLE)
	u.ID = bson.NewObjectId()
	u.Date = time.Now()
	return c.Insert(&u)
}

func (u *User) Destroy(db *mgo.Database) error {
	c := db.C(USER_TABLE)
	return c.Remove(&u)
}

func (u *User) Update(db *mgo.Database) error {
	c := db.C(USER_TABLE)
	return c.UpdateId(u.ID, u)
}

func GetUserByName(db *mgo.Database, name string) (*User, error) {
	c := db.C(USER_TABLE)
	user := new(User)
	err := c.Find(bson.M{"username": bson.M{"$eq": name}}).One(&user)
	return user, err
}

func GetUserByID(db *mgo.Database, id string) (*User, error) {
	c := db.C(USER_TABLE)
	user := new(User)
	if !bson.IsObjectIdHex(id) {
		return user, errors.New("user id is not correct")
	}
	err := c.FindId(bson.ObjectIdHex(id)).One(&user)
	return user, err
}

func JsonUsers(db *mgo.Database, opts *utils.OptPage, search bson.M) (*utils.PaginationJson, error) {
	c := db.C(USER_TABLE)
	q := c.Find(search)
	p, err := utils.Paginate(q, opts)
	if err != nil {
		return nil, err
	}
	var as = []User{}
	err = p.Query.Sort("-date").All(&as)
	var ajs = []UserJson{}
	for _, v := range as {
		aj := new(UserJson)
		aj.Set(&v)
		ajs = append(ajs, *aj)
	}
	pj := p.ToJson(ajs)
	return pj, err
}

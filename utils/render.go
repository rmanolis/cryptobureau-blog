package utils

import (
	"github.com/microcosm-cc/bluemonday"
	"github.com/unrolled/render"
)

var Sanitizer = bluemonday.UGCPolicy()

var Render = render.New(render.Options{
	Layout: "layout",
})

package utils

import (
	mgo "gopkg.in/mgo.v2"
	"strconv"
)

type Pagination struct {
	Page    int
	PerPage int
	Total   int
	Query   *mgo.Query
}

func (pa *Pagination) ToJson(objs interface{}) *PaginationJson {
	pj := new(PaginationJson)
	pj.Total = pa.Total
	pj.Page = pa.Page
	pj.PerPage = pa.PerPage
	pj.Objects = objs
	return pj
}

type PaginationJson struct {
	Page    int         `json:"page"`
	PerPage int         `json:"per_page"`
	Total   int         `json:"total"`
	Objects interface{} `json:"objects"`
}

type OptPage struct {
	Page    int
	PerPage int
}

func (o *OptPage) Set(page, per_page string) {
	var err error
	o.Page, err = strconv.Atoi(page)
	if err != nil {
		o.Page = 0
	}
	o.PerPage, err = strconv.Atoi(per_page)
	if err != nil {
		o.PerPage = 0 //for all
	}

}

func Paginate(q *mgo.Query, opts *OptPage) (*Pagination, error) {
	total, err := q.Count()
	if opts.PerPage == 0 {
		opts.PerPage = total
	}
	skip := opts.Page * opts.PerPage
	if err != nil {
		return nil, err
	}
	pa := new(Pagination)
	pa.Total = total
	pa.Page = opts.Page
	pa.PerPage = opts.PerPage
	pa.Query = q.Skip(skip).Limit(opts.PerPage)
	return pa, nil
}

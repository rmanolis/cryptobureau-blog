package utils

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gopkg.in/mgo.v2"
)

var (
	Session *mgo.Session
	Mongo   *mgo.DialInfo
)

func Connect(uri string) {
	mongo, err := mgo.ParseURL(uri)
	s, err := mgo.Dial(uri)
	if err != nil {
		fmt.Printf("Can't connect to mongo. Error: %s\n", err.Error())
		panic(err.Error())
	}
	s.SetSafe(&mgo.Safe{})
	fmt.Println("Connected to", uri)
	Session = s
	Mongo = mongo
}

func MgoMiddleware(c *gin.Context) {
	s := Session.Clone()
	defer s.Close()

	c.Set("db", s.DB(Mongo.Database))
	c.Next()
}

func DB(c *gin.Context) *mgo.Database {
	db := c.MustGet("db").(*mgo.Database)
	return db
}

package ctrls

import (
	"bytes"
	"encoding/json"
	"html/template"

	"log"
	"net/http"

	"strings"

	"bitbucket.org/rmanolis/cblib"
	"bitbucket.org/rmanolis/cryptobureau-blog/models"
	"bitbucket.org/rmanolis/cryptobureau-blog/utils"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func GetHtmlArticle(c *gin.Context) {
	db := utils.DB(c)
	aj := new(models.ArticleJson)
	id := c.Param("id")
	article, err := models.GetArticleByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	aj.Set(db, article)
	st, _ := JSONMarshal(map[string]string{"text": article.Text})
	utils.Render.HTML(c.Writer, http.StatusOK, "article", gin.H{
		"Title":      aj.Title,
		"Text":       template.HTML(aj.Text),
		"Username":   aj.Username,
		"PublicKey":  aj.PublicKey,
		"Signature":  aj.Signature,
		"UserID":     aj.UserID,
		"Date":       article.Date.Format("2006/01/02 15:04"),
		"SignedText": string(st),
		"Tags":       aj.Tags,
	})
}

func GetArticle(c *gin.Context) {
	db := utils.DB(c)
	aj := new(models.ArticleJson)
	id := c.Param("id")
	article, err := models.GetArticleByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	aj.Set(db, article)
	c.JSON(http.StatusOK, aj)
}

func GetArticles(c *gin.Context) {
	db := utils.DB(c)
	opt_page := new(utils.OptPage)
	opt_page.Set(c.Query("page"), c.Query("per_page"))
	query := bson.M{"signature": bson.M{"$ne": nil}}
	get_signed := c.Query("signed")
	if get_signed == "false" {
		query["signature"] = bson.M{"$eq": nil}
	}
	search := c.Query("search")
	if len(search) > 0 {
		query["title"] = bson.M{"$regex": search, "$options": "i"}
	}
	stags := c.Query("tags")
	tags := strings.Split(stags, ",")
	tag_ids := []bson.ObjectId{}
	for _, v := range tags {
		tag, err := models.GetTagByTitle(db, v)
		if err == nil {
			tag_ids = append(tag_ids, tag.ID)
		}
	}
	if len(tag_ids) > 0 {
		query["tags"] = bson.M{"$all": tag_ids}
	}

	ajs, _ := models.JsonArticles(db, opt_page, query)
	c.JSON(http.StatusOK, ajs)

}

func GetArticlesByUser(c *gin.Context) {
	db := utils.DB(c)
	user_id := c.Param("id")
	opt_page := new(utils.OptPage)
	opt_page.Set(c.Query("page"), c.Query("per_page"))
	query := bson.M{"signature": bson.M{"$ne": nil}}
	get_signed := c.Query("signed")
	if get_signed == "false" {
		query["signature"] = bson.M{"$eq": nil}
	}
	if !bson.IsObjectIdHex(user_id) {
		c.JSON(http.StatusNotFound, "user is not found")
		return
	}
	user, err := models.GetUserByID(db, user_id)
	if err != nil {
		c.JSON(http.StatusNotFound, "user is not found")
		return
	}
	query["user_id"] = bson.M{"$eq": user.ID}
	ajs, _ := models.JsonArticles(db, opt_page, query)
	c.JSON(http.StatusOK, ajs)
}

func GetArticlesByTag(c *gin.Context) {
	db := utils.DB(c)
	tag_id := c.Param("id")
	opt_page := new(utils.OptPage)
	opt_page.Set(c.Query("page"), c.Query("per_page"))
	query := bson.M{"signature": bson.M{"$ne": nil}}
	get_signed := c.Query("signed")
	if get_signed == "false" {
		query["signature"] = bson.M{"$eq": nil}
	}
	if !bson.IsObjectIdHex(tag_id) {
		c.JSON(http.StatusNotFound, "tag is not found")
		return
	}
	tag, err := models.GetTagByID(db, tag_id)
	if err != nil {
		c.JSON(http.StatusNotFound, "tag is not found")
		return
	}
	query["tags"] = bson.M{"$all": []bson.ObjectId{tag.ID}}
	ajs, _ := models.JsonArticles(db, opt_page, query)
	c.JSON(http.StatusOK, ajs)
}

func AddArticle(c *gin.Context) {
	db := utils.DB(c)
	aj := models.ArticleJson{}
	err := c.BindJSON(&aj)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	if len(aj.Title) == 0 {
		c.JSON(http.StatusUnauthorized, "title is empty")
		return
	}
	if len(aj.Text) == 0 {
		c.JSON(http.StatusUnauthorized, "text is empty")
		return
	}

	user, err := models.GetUserByName(db, aj.Username)
	if err != nil {
		c.JSON(http.StatusNotFound, "username does not exist")
		return
	}
	article := new(models.Article)
	article.Title = aj.Title
	article.Text = utils.Sanitizer.Sanitize(aj.Text)
	article.UserID = user.ID
	article.Tags = []bson.ObjectId{}
	for _, v := range aj.Tags {
		tag, err := models.GetTagByTitle(db, v)
		if err != nil {
			c.JSON(http.StatusNotFound, "tag with title "+v+" does not exist")
			return
		}
		article.Tags = append(article.Tags, tag.ID)
	}
	err = article.Save(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "username already exists")
		return
	}
	aj.Set(db, article)
	c.JSON(http.StatusAccepted, aj)
}

func JSONMarshal(v interface{}) ([]byte, error) {
	b, err := json.Marshal(v)

	b = bytes.Replace(b, []byte("\\u003c"), []byte("<"), -1)
	b = bytes.Replace(b, []byte("\\u003e"), []byte(">"), -1)
	b = bytes.Replace(b, []byte("\\u0026"), []byte("&"), -1)

	return b, err
}

func VerifyArticle(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	id := c.Param("id")
	article, err := models.GetArticleByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	ats := new(models.ArticleToSignJson)
	ats.Set(article)
	b, err := JSONMarshal(ats)
	if err != nil {
		article.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	log.Println(string(b))

	if sj.Text != string(b) {
		article.Destroy(db)
		c.JSON(http.StatusUnauthorized, "text is not the same for the article also")
		return
	}

	user, err := models.GetUserByID(db, article.UserID.Hex())
	if err != nil {
		article.Destroy(db)
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		article.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if !is_verified {
		article.Destroy(db)
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	article.Signature = sj.Signature
	err = article.Update(db)
	if err != nil {
		article.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	aj := new(models.ArticleJson)
	aj.Set(db, article)
	c.JSON(http.StatusAccepted, aj)

}

func DeleteArticle(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	id := c.Param("id")
	article, err := models.GetArticleByID(db, id)
	if err != nil {
		c.JSON(http.StatusNotFound, "article not found")
		return
	}
	if len(sj.Username) == 0 {
		c.JSON(http.StatusUnauthorized, "username is empty")
		return
	}

	if sj.Text != article.ID.Hex()+"-delete" {
		c.JSON(http.StatusUnauthorized, "text is not the same with the article's text")
		return
	}
	user, err := models.GetUserByName(db, sj.Username)
	if err != nil {
		c.JSON(http.StatusNotFound, "user not found")
		return
	}
	if user.ID != article.UserID && !user.IsAdmin {
		c.JSON(http.StatusUnauthorized, "you are not a writer or the admin")
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if !is_verified {
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	err = article.Destroy(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.AbortWithStatus(http.StatusNoContent)
}

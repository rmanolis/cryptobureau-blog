package ctrls

import (
	"net/http"
	"strings"

	"bitbucket.org/rmanolis/cblib"
	"bitbucket.org/rmanolis/cryptobureau-blog/models"
	"bitbucket.org/rmanolis/cryptobureau-blog/utils"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func GetTags(c *gin.Context) {
	db := utils.DB(c)
	opt_page := new(utils.OptPage)
	opt_page.Set(c.Query("page"), c.Query("per_page"))
	query := bson.M{"signature": bson.M{"$ne": nil}}
	get_signed := c.Query("signed")
	if get_signed == "false" {
		query["signature"] = bson.M{"$eq": nil}
	}
	search := c.Query("search")
	if len(search) > 0 {
		query["title"] = bson.M{"$regex": search, "$options": "i"}
	}

	ajs, _ := models.JsonTags(db, opt_page, query)
	if c.Query("onlyObjects") == "true" {
		c.JSON(http.StatusOK, ajs.Objects)
		return
	}
	c.JSON(http.StatusOK, ajs)

}

func GetTag(c *gin.Context) {
	db := utils.DB(c)
	tj := new(models.TagJson)
	id := c.Param("id")
	tag, err := models.GetTagByID(db, id)
	if err != nil {
		tag, err = models.GetTagByTitle(db, id)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			return
		}
	}
	tj.Set(tag)
	c.JSON(http.StatusOK, tj)
}

func AddTag(c *gin.Context) {
	db := utils.DB(c)
	tj := models.TagJson{}
	err := c.BindJSON(&tj)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	if len(tj.Text) == 0 {
		c.JSON(http.StatusUnauthorized, "tag's name is empty")
		return
	}
	if len(tj.Username) == 0 {
		c.JSON(http.StatusUnauthorized, "username is empty")
		return
	}
	user, err := models.GetUserByName(db, tj.Username)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "username does not exist")
		return
	}
	if strings.Contains(tj.Text, "\n") {
		c.JSON(http.StatusUnauthorized, "title contains a new line")
		return
	}
	tag := new(models.Tag)
	tag.Title = tj.Text
	tag.UserID = user.ID
	err = tag.Save(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "username already exists")
		return
	}
	tj.Set(tag)
	c.JSON(http.StatusAccepted, tj)
}

func VerifyTag(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	id := c.Param("id")
	tag, err := models.GetTagByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}

	if sj.Text != tag.Title {
		tag.Destroy(db)
		c.JSON(http.StatusUnauthorized, "text is not the same with the tag's title")
		return
	}

	user, err := models.GetUserByID(db, tag.UserID.Hex())
	if err != nil {
		tag.Destroy(db)
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}

	if !user.IsAdmin {
		tag.Destroy(db)
		c.JSON(http.StatusUnauthorized, "user is not an admin")
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		tag.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if !is_verified {
		tag.Destroy(db)
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	tag.Signature = sj.Signature
	err = tag.Update(db)
	if err != nil {
		tag.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	tj := new(models.TagJson)
	tj.Set(tag)
	c.JSON(http.StatusAccepted, tj)
}

func DeleteTag(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	id := c.Param("id")
	tag, err := models.GetTagByID(db, id)
	if err != nil {
		c.JSON(http.StatusNotFound, "tag not found")
		return
	}
	if len(sj.Username) == 0 {
		c.JSON(http.StatusUnauthorized, "username is empty")
		return
	}

	if sj.Text != tag.ID.Hex()+"-delete" {
		c.JSON(http.StatusUnauthorized, "text is not the same with the tag's id")
		return
	}
	user, err := models.GetUserByName(db, sj.Username)
	if err != nil {
		c.JSON(http.StatusNotFound, "user not found")
		return
	}
	if !user.IsAdmin {
		c.JSON(http.StatusUnauthorized, "you are not an admin")
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if !is_verified {
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	err = tag.Destroy(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	c.AbortWithStatus(http.StatusNoContent)
}

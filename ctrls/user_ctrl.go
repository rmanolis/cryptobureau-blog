package ctrls

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/rmanolis/cblib"
	"bitbucket.org/rmanolis/cryptobureau-blog/models"
	"bitbucket.org/rmanolis/cryptobureau-blog/utils"
	"github.com/gin-gonic/gin"
)

func GetUsers(c *gin.Context) {
	db := utils.DB(c)
	opt_page := new(utils.OptPage)
	opt_page.Set(c.Query("page"), c.Query("per_page"))
	query := bson.M{"signature": bson.M{"$ne": nil}}
	get_signed := c.Query("signed")
	if get_signed == "false" {
		query["signature"] = bson.M{"$eq": nil}
	}
	search := c.Query("search")
	if len(search) > 0 {
		query["username"] = bson.M{"$regex": search, "$options": "i"}
	}

	ajs, _ := models.JsonUsers(db, opt_page, query)
	c.JSON(http.StatusOK, ajs)

}

func GetUser(c *gin.Context) {
	db := utils.DB(c)
	uj := new(models.UserJson)
	id := c.Param("id")
	user, err := models.GetUserByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err)
		return
	}
	uj.Set(user)
	c.JSON(http.StatusOK, uj)
}

func RegisterUser(c *gin.Context) {
	db := utils.DB(c)
	uj := models.UserJson{}
	err := c.BindJSON(&uj)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	if len(uj.Username) == 0 {
		c.JSON(http.StatusUnauthorized, "username is empty")
		return
	}
	if len(uj.PublicKey) == 0 {
		c.JSON(http.StatusUnauthorized, "public key is empty")
		return
	}

	u, err := models.GetUserByName(db, uj.Username)
	if err == nil {
		c.JSON(http.StatusUnauthorized, "username already exists")
		return
	}
	u.PublicKey = uj.PublicKey
	u.Username = uj.Username
	u.IsAdmin = false
	err = u.Save(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, "username already exists")
		return
	}
	uj.ID = u.ID.Hex()
	c.JSON(http.StatusAccepted, uj)
}

func VerifyUser(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}
	id := c.Param("id")
	user, err := models.GetUserByID(db, id)
	if err != nil {
		c.JSON(http.StatusUnauthorized, err.Error())
		return
	}

	if sj.Text != user.Username {
		c.JSON(http.StatusUnauthorized, "text is not the same with the username")
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		user.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	if !is_verified {
		user.Destroy(db)
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	user.Signature = sj.Signature
	err = user.Update(db)
	if err != nil {
		user.Destroy(db)
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}

	uj := new(models.UserJson)
	uj.Set(user)
	c.JSON(http.StatusAccepted, uj)
}

func DeleteUser(c *gin.Context) {
	db := utils.DB(c)
	sj := models.SignatureJson{}
	err := c.BindJSON(&sj)
	id := c.Param("id")
	delete_user, err := models.GetUserByID(db, id)
	if err != nil {
		c.JSON(http.StatusNotFound, "user to delete not found")
		return
	}
	if len(sj.Username) == 0 {
		c.JSON(http.StatusUnauthorized, "username is empty")
		return
	}

	if sj.Text != delete_user.ID.Hex()+"-delete" {
		c.JSON(http.StatusUnauthorized, "text is not the same with the user's id")
		return
	}

	user, err := models.GetUserByName(db, sj.Username)
	if err != nil {
		c.JSON(http.StatusNotFound, "user not found")
		return
	}
	if delete_user.IsAdmin {
		c.JSON(http.StatusNotFound, "you can not delete an admin")
		return
	}
	if user.ID != delete_user.ID && !user.IsAdmin {
		c.JSON(http.StatusUnauthorized, "you are not the user or the admin")
		return
	}

	is_verified, err := cblib.Verify([]byte(user.PublicKey), []byte(sj.Text), []byte(sj.Signature))
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	if !is_verified {
		c.JSON(http.StatusInternalServerError, "signature is not correct")
		return
	}

	err = delete_user.Destroy(db)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
		return
	}

	c.AbortWithStatus(http.StatusNoContent)
}

package ctrls

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func LoadRoutes(r *gin.Engine) {
	r.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/init.cb")
	})
	r.GET("/init.cb", func(c *gin.Context) {
		c.File("./static/index.html")
	})
	r.GET("/articles/:id/show.cb", GetHtmlArticle)
	api := r.Group("/api")
	api.GET("/users", GetUsers)
	api.GET("/users/:id", GetUser)
	api.GET("/users/:id/articles", GetArticlesByUser)
	api.PUT("/users/:id/delete", DeleteUser)
	api.POST("/register", RegisterUser)
	api.PUT("/register/:id", VerifyUser)

	api.GET("/articles", GetArticles)
	api.GET("/articles/:id", GetArticle)
	api.POST("/articles", AddArticle)
	api.PUT("/articles/:id", VerifyArticle)
	api.PUT("/articles/:id/delete", DeleteArticle)

	api.GET("/tags", GetTags)
	api.GET("/tags/:id", GetTag)
	api.GET("/tags/:id/articles", GetArticlesByTag)
	api.POST("/tags", AddTag)
	api.PUT("/tags/:id", VerifyTag)
	api.PUT("/tags/:id/delete", DeleteTag)

}

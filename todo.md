x Model User, Article, Tag

# Register
  x Add public key
  x Sign user id to verify public key

# Post Article
  x Add title, essay and your username
  x Sign Json {title, essay, username} 
  - list signed tags and select

# Delete Article
  x Sign ArticleID with random text 

# List articles (if user is admin, add delete button)
  - List by user 
  - List by choosen tags
  - List new ones
  - List no signed

# Show article
  x Show article

## Admin Panel

# Tags
  - List tags by date
  - List no signed tags

# Tag
  x Add tag by admin
   x tag title will have not new line
  x verify tag
  x remove tag by admin

# Users
  x Delete user 
    - delete with their articles
    x dont delete admins



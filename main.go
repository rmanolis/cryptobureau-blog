package main

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/rmanolis/cryptobureau-blog/ctrls"
	"bitbucket.org/rmanolis/cryptobureau-blog/models"
	"bitbucket.org/rmanolis/cryptobureau-blog/utils"

	"github.com/gin-gonic/gin"

	"github.com/pelletier/go-toml"
)

func main() {
	initializeConfig()
	ticker := time.NewTicker(time.Second * 30)
	go func() {
		fmt.Println("started ticket")
		db := utils.Session.Clone().DB(utils.Mongo.Database)
		for {
			select {
			case <-ticker.C:
				models.DeleteUnsignedArticles(db)
			}
		}
	}()
	r := gin.Default()
	r.Use(utils.MgoMiddleware)
	r.Static("/static", "./static")
	r.Static("/node_modules", "./static/app/node_modules")

	ctrls.LoadRoutes(r)
	r.Run() // listen and server on 0.0.0.0:8080
}

func initializeConfig() {
	config, err := toml.LoadFile("config.toml")
	if err != nil {
		fmt.Printf("Can't find config file \"config.toml\". Error: %s\n", err.Error())
		panic(err.Error())

	}

	public_key, exists := config.Get("admin.publickey").(string)
	if !exists {
		fmt.Println("Can't find admin's public key in the config file")
		panic(errors.New("Can't find admin's public key in the config file"))
	}

	mongo_uri, exists := config.Get("mongodb").(string)
	if !exists {
		fmt.Println("Can't find Mongodb's URI in the config file")
		panic(errors.New("Can't find Mongodb's URI in the config file"))
	}

	utils.Connect(mongo_uri)
	s := utils.Session.Clone()
	defer s.Close()
	db := s.DB(utils.Mongo.Database)
	admin, err := models.GetUserByName(db, "admin")
	if err != nil {
		admin.PublicKey = public_key
		admin.Username = "admin"
		admin.IsAdmin = true
		admin.Save(db)
	} else {
		if public_key != admin.PublicKey {
			admin.PublicKey = public_key
			admin.Update(db)
		}
	}

}

module.exports =  ["$http",
    function($http){
  var obj = {};

 
  obj.getUser = function(id){
    return $http.get('/api/users/'+id);
  }

   
  obj.getUsers = function(per_page, page){
    var q = '/api/users?';
    if(per_page){
      q += 'per_page='+per_page+'&';
    }
    if(page){
      q += 'page='+page;
    }

    return $http.get(q);
  }



  obj.register = function(data){
    return $http.post('/api/register', data);
  }

  obj.verifyUser= function(id,data){
    return $http.put('/api/register/'+id, data);
  }

 
  obj.deleteUser = function(id,data){
    return $http.put('/api/users/'+id+'/delete', data);
  }
  
  obj.adminUser = function(id,data){
    return $http.put('/api/users/'+id+'/admin', data);
  }


  
  return obj;

}];

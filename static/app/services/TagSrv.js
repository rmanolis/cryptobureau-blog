module.exports =  ["$http",
    function($http){
  var obj = {};

 
  obj.getTag = function(id){
    return $http.get('/api/tags/'+id);
  }



  obj.getTags = function(id){
    return $http.get('/api/tags');
  }

  obj.autocompleteTags = function(query){
    return $http.get('/api/tags?perPage=7&onlyObjects=true&search=' + query);
  }


  obj.addTag = function(data){
    return $http.post('/api/tags', data);
  }

  obj.verifyTag = function(id,data){
    return $http.put('/api/tags/'+id, data);
  }

  obj.deleteTag= function(id,data){
    return $http.put('/api/tags/'+id + '/delete', data);
  }

  
  return obj;

}];

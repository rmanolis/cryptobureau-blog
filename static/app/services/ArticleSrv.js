module.exports = ["$http",
  function($http){
  var obj = {};

  obj.getArticles = function(tags,per_page,page){
    var q = '/api/articles?';
    if(tags){
      var ts = tags.map(tag => { return tag.text})
      q += 'tags='+ ts + '&';
    }
    if(per_page){
      q += 'per_page='+per_page+'&';
    }
    if(page){
      q += 'page='+page;
    }
    console.log(q);
    return $http.get(q);
  }

 
  obj.getArticle = function(id){
    return $http.get('/api/articles/'+id);
  }

  obj.getArticlesByUser = function(user_id, per_page, page){
    var q = '/api/users/'+user_id+'/articles?'
    if(per_page){
      q += 'per_page='+per_page+'&';
    }
    if(page){
      q += 'page='+page;
    }

    return $http.get(q);

  }

  obj.getArticlesByTag = function(tag_id, per_page, page){
    var q = '/api/tags/'+tag_id+'/articles?'
    if(per_page){
      q += 'per_page='+per_page+'&';
    }
    if(page){
      q += 'page='+page;
    }

    return $http.get(q);
  }


  obj.addArticle = function(data){
    return $http.post('/api/articles', data);
  }

  obj.verifyArticle= function(id,data){
    return $http.put('/api/articles/'+id, data);
  }

  obj.deleteArticle= function(id,data){
    return $http.put('/api/articles/'+id + '/delete', data);
  }

  
  return obj;

}];

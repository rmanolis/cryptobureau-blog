var css = require('./app.css');

var angular = require('angular');
require('angular-route');
require('angular-ui-bootstrap');
require('angular-sanitize');
require('angular-animate');
require('angular-toastr');
require('trix')
require('angular-trix')
require('ng-tags-input');
require('ng-meta');
var app = angular.module('App', ['ngRoute', 'ngTagsInput','ui.bootstrap','angularTrix',
                         'ngSanitize','ngAnimate','toastr','ngMeta']);

require('./services');
require('./ctrls');


app.run(function($rootScope,$http,ngMeta){
  ngMeta.init();
});


app.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/static/app/pages/main.html',
      controller: 'MainCtrl'
    })

    .when('/register', {
      templateUrl: '/static/app/pages/register.html',
      controller: 'RegisterCtrl'
    })

    .when('/users', {
      templateUrl: '/static/app/pages/list_users.html',
      controller: 'ListUsersCtrl'
    })


    .when('/users/:id', {
      templateUrl: '/static/app/pages/show_user.html',
      controller: 'ShowUserCtrl'
    })



    .when('/articles/:id', {
      templateUrl: '/static/app/pages/show_article.html',
      controller: 'ShowArticleCtrl'
    })
    
    .when('/add/article', {
      templateUrl: '/static/app/pages/add_article.html',
      controller: 'AddArticleCtrl'
    })
    
    
    .when('/tags/:id', {
      templateUrl: '/static/app/pages/show_tag.html',
      controller: 'ShowTagCtrl'
    })
    
    .when('/add/tag', {
      templateUrl: '/static/app/pages/add_tag.html',
      controller: 'AddTagCtrl'
    })



 
 
 
});


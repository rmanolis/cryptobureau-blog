module.exports = ["$scope", "$location", '$routeParams','toastr',"TagSrv","ArticleSrv",
    function($scope, $location, $routeParams, toastr, TagSrv,ArticleSrv){
    $scope.isDeleting = false;
    $scope.article = {
      id : "",
      title : ""
    }
    var id = $routeParams.id;
    $scope.perPage = 2;
    $scope.currentPage = 1;
    $scope.totalArticles = 0;
  
    $scope.pageChanged = function() {
      ArticleSrv.getArticlesByTag($scope.tag.id,  $scope.perPage, $scope.currentPage - 1).success(function(resp){
        console.log(resp);
        $scope.totalArticles = resp.total;
        $scope.articles = resp.objects;
      });
    };

    TagSrv.getTag(id).success(function(tag){
      $scope.tag = tag;
      $scope.pageChanged();
    }).error(function(data){
      $location.path('/')
    })

    $scope.delete = function(){
      $scope.isDeleting = true;
    }

    $scope.verify = function(tag, username, signature){
      TagSrv.deleteTag(tag.id, {text: tag.id, username: username, signature: signature}).success(function(){
        $location.path('/');
      }).error(function(data){
        toastr.error(data);
      })
    }
    
}]

module.exports = ["$scope","UserSrv","ArticleSrv","TagSrv",
    function($scope, UserSrv,ArticleSrv,TagSrv){
    $scope.tags = [];
    $scope.articles = [];
    $scope.perPage = 2;
    $scope.currentPage = 1;
    $scope.totalArticles = 0;
  
    $scope.pageChanged = function() {
      ArticleSrv.getArticles($scope.tags,  $scope.perPage, $scope.currentPage - 1).success(function(resp){
        $scope.totalArticles = resp.total;
        $scope.articles = resp.objects;
      });

    };

    ArticleSrv.getArticles($scope.tags,  $scope.perPage, $scope.currentPage - 1).success(function(resp){
      $scope.totalArticles = resp.total;
      $scope.articles = resp.objects;
    });

    $scope.loadTags = function(query) {
      return TagSrv.autocompleteTags(query);
    };

    $scope.searchArticlesByTags = function(tags){
      ArticleSrv.getArticles(tags,$scope.perPage).success(function(resp){
        $scope.totalArticles = resp.total;        
        $scope.articles = resp.objects;
      });
    }

}]

module.exports = ["$scope", "$location", '$routeParams','toastr',"UserSrv","ArticleSrv",
    function($scope, $location, $routeParams,toastr, UserSrv,ArticleSrv){
    $scope.isDeleting = false;
    $scope.user = {
      id : "",
      username : "",
      public_key: "",
    }
    var id = $routeParams.id;
    $scope.perPage = 2;
    $scope.currentPage = 1;
    $scope.totalArticles = 0;
  
    $scope.pageChanged = function() {
      ArticleSrv.getArticlesByUser($scope.user.id,  $scope.perPage, $scope.currentPage - 1).success(function(resp){
        console.log(resp);
        $scope.totalArticles = resp.total;
        $scope.articles = resp.objects;
      });
    };

    UserSrv.getUser(id).success(function(user){
      $scope.user = user;
      $scope.pageChanged();
    }).error(function(data){
      $location.path('/')
    })

    $scope.delete = function(){
      $scope.isDeleting = true;
    }

    $scope.verify = function(user, username, signature){
      UserSrv.deleteUser(user.id, {text: user.id, username: username, signature: signature}).success(function(){
        $location.path('/');
      }).error(function(data){
        toastr.error(data);
      })
    }

    $scope.articles = [];
    ArticleSrv.getArticlesByUser(id).success(function(data){
      $scope.articles = data.objects;
    })

    
}]

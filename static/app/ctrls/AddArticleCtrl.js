module.exports = ["$scope", "$location","$http","toastr", "ArticleSrv","TagSrv",
    function($scope, $location, $http,toastr, ArticleSrv , TagSrv){
    $scope.isVerifying = false
    $scope.article = {
      title : "",
      text : "",
      username : "",
    }
    $scope.article_to_sign = "";

    $scope.addArticle = function(article, tags){
      if(tags){
        article.tags = [];
        for(let tag of tags){
          article.tags.push(tag.text);
        }
      }
      ArticleSrv.addArticle(article).success(function(article){
        $scope.isVerifying = true;
        console.log(article);
        $scope.article = article;
        $scope.article_to_sign = JSON.stringify({text: article.text})
      }).error(function(data){
        toastr.error(data);
      })
    }

    $scope.verify = function(article, ats, signature){
      ArticleSrv.verifyArticle(article.id, {text:ats, signature:signature}).success(function(){
        $location.path('/articles/'+article.id)  
      }).error(function(data){
        toastr.error(data);
      })

    }


    $scope.loadTags = function(query) {
      return TagSrv.autocompleteTags(query);
    };
}]

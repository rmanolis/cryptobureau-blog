module.exports = ["$scope", "$location", '$routeParams',"toastr","ngMeta","ArticleSrv",
    function($scope, $location, $routeParams, toastr, ngMeta, ArticleSrv){
    $scope.isDeleting = false;
    $scope.article = {
      id : "",
      title : "",
      text : "",
      public_key: "",
      username : "",
      signed_text: "",
      tags:[],
    }
    var id = $routeParams.id;
    ArticleSrv.getArticle(id).success(function(article){
      $scope.article = article;
      $scope.article.signed_text = JSON.stringify({title:article.title,text:article.text});
    }).error(function(data){
      $location.path('/')
    })


    $scope.delete = function(){
      $scope.isDeleting = true;
    }

    $scope.verify = function(article, username, signature){
      ArticleSrv.deleteArticle(article.id, {text: article.id, 
                               username: username, signature: signature}).success(function(){
        $location.path('/');
      }).error(function(data){
        toastr.error(data);
      })
    }
    
}]

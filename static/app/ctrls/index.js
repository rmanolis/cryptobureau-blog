var app = require('angular').module('App');

app.controller("AddArticleCtrl",require('./AddArticleCtrl.js'));
app.controller("AddTagCtrl",require('./AddTagCtrl.js'));
app.controller("ListUsersCtrl",require('./ListUsersCtrl.js'));
app.controller("MainCtrl",require('./MainCtrl.js'));
app.controller("RegisterCtrl", require('./RegisterCtrl.js'));
app.controller("ShowArticleCtrl", require('./ShowArticleCtrl.js'));
app.controller("ShowTagCtrl",require('./ShowTagCtrl.js'));
app.controller("ShowUserCtrl",require('./ShowUserCtrl.js'));



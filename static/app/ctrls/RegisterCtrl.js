module.exports = ["$scope", "$location","toastr", "UserSrv",
    function($scope, $location,toastr, UserSrv){
    $scope.isVerifying = false
    $scope.user = {
      public_key : "",
      username : ""
    }

  
    
    $scope.register = function(user){
      UserSrv.register(user).success(function(user){
        $scope.isVerifying = true;
        console.log(user);
        $scope.user = user;
      }).error(function(data){
        toastr.error(data);
      })
    }

    $scope.verify = function(user, signature){
      UserSrv.verifyUser(user.id, {text:user.username, signature:signature}).success(function(){
        $location.path('/users/'+user.id)  
      }).error(function(data){
        toastr.error(data);
      })

    }
}]

module.exports = ["$scope","toastr","UserSrv","ArticleSrv","TagSrv",
  function($scope,toastr, UserSrv,ArticleSrv,TagSrv){
  $scope.isDeleting = false;    
  $scope.users = [];
  $scope.selectedUser = null;
  $scope.perPage = 20;
  $scope.currentPage = 1;
  $scope.totalUsers = 0;

  $scope.pageChanged = function() {
    UserSrv.getUsers($scope.perPage, $scope.currentPage - 1).success(function(resp){
      console.log(resp);
      $scope.totalUsers = resp.total;
      $scope.users = resp.objects;
    });
  };
  $scope.pageChanged();
  $scope.delete = function(user){
    $scope.isDeleting = true;
    $scope.selectedUser = user;
  }

  $scope.verify = function(user, username, signature){
    UserSrv.deleteUser(user.id, {text: user.id+"-delete", username: username, signature: signature}).success(function(){
      $scope.pageChanged();
      $scope.isDeleting = false;
      $scope.selectedUser = null;      
    }).error(function(data){
      toastr.error(data);
    })
  }


}]

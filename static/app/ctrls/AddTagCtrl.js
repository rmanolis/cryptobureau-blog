module.exports = ["$scope", "$location","toastr", "TagSrv",
    function($scope, $location,toastr, TagSrv){
    $scope.isVerifying = false
    $scope.tag = {
      title : "",
      username : "",
    }

    $scope.add = function(tag){
      TagSrv.addTag(tag).success(function(tag){
        $scope.isVerifying = true;
        console.log(tag);
        $scope.tag = tag;
      }).error(function(data){
        toastr.error(data);
      })
    }

    $scope.verify = function(tag, signature){
      TagSrv.verifyTag(tag.id, {text:tag.title, signature:signature}).success(function(){
        $location.path('/tags/'+tag.id)  
      }).error(function(data){
        toastr.error(data);
      })

    }
}]
